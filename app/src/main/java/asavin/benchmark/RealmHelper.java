package asavin.benchmark;

import io.realm.Realm;

/**
 * Created by 1 on 21.07.2016.
 */
public class RealmHelper
{
    Realm realm;
    public void save(final PersonRealm personRealm)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm r)
            {
                realm.copyToRealm(personRealm);
                realm.commitTransaction();
            }
        });

    }
}
