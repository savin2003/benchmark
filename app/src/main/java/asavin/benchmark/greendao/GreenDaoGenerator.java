package asavin.benchmark.greendao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by 1 on 20.07.2016.
 */
public class GreenDaoGenerator
{
    private static final int VERSION = 1;
    private static final String PACKAGE_DESTINATION = "asavin.benchmark.greendao";
    private static final String DESTINATION_FOLDER = "./app/src/main/java";

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(VERSION,PACKAGE_DESTINATION);
        Entity entity = createUserEntity(schema);

        new DaoGenerator().generateAll(schema,DESTINATION_FOLDER);

//        DaoMaster daoMaster = new DaoMaster();
    }

    private static Entity createUserEntity(Schema schema) {
        Entity userEntity = schema.addEntity("PersonOrm");
        userEntity.addIdProperty().primaryKey().autoincrement();
        userEntity.addStringProperty("name");
        userEntity.addStringProperty("secondName");

        return userEntity;
    }
}
