package asavin.benchmark;


/**
 * Created by 1 on 20.07.2016.
 */
public class Person
{
    private int id;
    private String name;
    private String secondName;

    public Person(){
    }

    public Person(int id, String name, String secondName) {

        this.id = id;
        this.name = name;
        this.secondName = secondName;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
