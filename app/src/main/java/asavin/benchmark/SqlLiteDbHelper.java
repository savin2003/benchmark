package asavin.benchmark;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 1 on 20.07.2016.
 */
public class SqlLiteDbHelper extends SQLiteOpenHelper{
    public SqlLiteDbHelper(Context context){
        super(context, "myDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table simplePersonTable ("
                + "_id integer primary key autoincrement,"
                + "name text,"
                + "secondName text" + ");");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }
}
