package asavin.benchmark;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PersonRealm extends RealmObject
{
    public PersonRealm()
    {}
    public int getId()
    {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @PrimaryKey
    private int id;
    private String name;

    public PersonRealm(int id, String name, String secondName) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
    }

    public String getSecondName() {

        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    private String secondName;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
