package asavin.benchmark;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.DoubleBuffer;
import android.os.Handler;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.logging.LogRecord;

import asavin.benchmark.greendao.DaoMaster;
import asavin.benchmark.greendao.DaoSession;
import asavin.benchmark.greendao.PersonOrm;
import asavin.benchmark.greendao.PersonOrmDao;

public class MainActivity extends Activity
{
    EditText editText;
    public long currentTime;
    int col;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.insertCol);

    }
    public void insert(View v)
    {
        DbTester.clearAll(this);
        if(!expectINputCol()) {
            Toast.makeText(this, "Неправильно введены данные", Toast.LENGTH_SHORT);
            return;
        }
        long sqlTime = System.currentTimeMillis();
        DbTester.sqlInsert(this,col);
        sqlTime = System.currentTimeMillis()-sqlTime;
        TextView tv = (TextView)findViewById(R.id.sqlRes);
        tv.setText(col + " записей вставлены ,используя sql.Время в миллисекундах: " + sqlTime);

        long ormTime = System.currentTimeMillis();
        DbTester.greenOrmInsert(this,col);
        ormTime = System.currentTimeMillis()-ormTime;
        tv = (TextView) findViewById(R.id.ormRes);
        tv.setText(col + " записей вставлены ,используя greenDao(ORM).Время в миллисекундах: " + ormTime);

        long realmTime = System.currentTimeMillis();
        DbTester.realmInsert(this, col);
        realmTime = System.currentTimeMillis()-realmTime;
        tv = (TextView) findViewById(R.id.realmRes);
        tv.setText(col + "  записей вставлены ,используя realm.Время в миллисекундах:  " + realmTime);

        GraphView graphView = (GraphView) findViewById(R.id.mainGraphView);
        graphView.removeAllSeries();
        BarGraphSeries<DataPoint> series = new BarGraphSeries <DataPoint>(new DataPoint[] {
                new DataPoint(0, sqlTime),
                new DataPoint(1, ormTime),
                new DataPoint(2, realmTime),
        });
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data)
            {
                return Color.rgb((int) data.getX() * 255 / 4, (int) Math.abs(data.getY() * 255 / 6), 100);
            }
        });
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.RED);
        graphView.setBackgroundColor(Color.LTGRAY);
        graphView.addSeries(series);
    }
    public void select(View v)
    {
        DbTester.clearAll(this);
        if(!expectINputCol()) {
            Toast.makeText(this, "Неправильно введены данные", Toast.LENGTH_SHORT);
            return;
        }
        long sqlTime = System.currentTimeMillis();
        DbTester.sqlRead(this);
        sqlTime = System.currentTimeMillis()-sqlTime;
        TextView tv = (TextView)findViewById(R.id.sqlRes);
        tv.setText(col + " записей выьраны ,используя sql.Время в миллисекундах: " + sqlTime);

        long ormTime = System.currentTimeMillis();
        DbTester.greenOrmRead(this);
        ormTime = System.currentTimeMillis()-ormTime;
        tv = (TextView) findViewById(R.id.ormRes);
        tv.setText(col + " записей вставлены ,используя greenDao(ORM).Время в миллисекундах: " + ormTime);

        long realmTime = System.currentTimeMillis();
        DbTester.realmRead(this);
        realmTime = System.currentTimeMillis()-realmTime;
        tv = (TextView) findViewById(R.id.realmRes);
        tv.setText(col + " записей вставлены ,используя realm.Время в миллисекундах: " + realmTime);

        GraphView graphView = (GraphView) findViewById(R.id.mainGraphView);
        graphView.removeAllSeries();
        BarGraphSeries<DataPoint> series = new BarGraphSeries <DataPoint>(new DataPoint[] {
                new DataPoint(0, sqlTime),
                new DataPoint(1, ormTime),
                new DataPoint(2, realmTime),
        });
        series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                return Color.rgb((int) data.getX() * 255 / 4, (int) Math.abs(data.getY() * 255 / 6), 100);
            }
        });
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.RED);
        graphView.setBackgroundColor(Color.LTGRAY);
        graphView.addSeries(series);
    }
    boolean expectINputCol()
    {
        boolean res;
        try
        {
            int col = Integer.parseInt(editText.getText().toString());
            res = true;
            this.col = col;
        }
        catch (NumberFormatException e)
        {
            res = false;
        }
        return res;
    }
}
