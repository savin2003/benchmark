package asavin.benchmark;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import asavin.benchmark.greendao.DaoMaster;
import asavin.benchmark.greendao.DaoSession;
import asavin.benchmark.greendao.PersonOrm;
import asavin.benchmark.greendao.PersonOrmDao;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by 1 on 21.07.2016.
 */
public class DbTester {
    public static void sqlInsert(Context context, int col) {
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        for (int i = 0; i < col; i++) {
            Person p = new Person(i, "name", "sName");
            ContentValues values = new ContentValues();
            values.put("name", p.getName());
            values.put("secondName", p.getSecondName());
            database.insert("simplePersonTable", null, values);
        }
    }
    public static void greenOrmInsert(Context context, int col) {
        SQLiteDatabase db = new DaoMaster.DevOpenHelper(context, "persons-db", null).getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        PersonOrmDao personOrmDao = daoSession.getPersonOrmDao();
        for (int i = 0; i < col; i++) {
            PersonOrm personOrm = new PersonOrm();
            personOrm.setName("name");
            personOrm.setSecondName("secondName");
            personOrmDao.insertOrReplace(personOrm);
        }
    }
    public static void realmInsert(Context context,int col) {
        RealmConfiguration configuration = new RealmConfiguration.Builder(context).build();
        Realm realm = Realm.getInstance(configuration);
        for(int i = 0;i<col;i++) {
            realm.beginTransaction();
            PersonRealm personRealm = new PersonRealm(i,"name","secondName");
            realm.copyToRealm(personRealm);
            realm.commitTransaction();
        }
    }
    public static ArrayList<PersonRealm> realmRead(Context context) {
        RealmConfiguration configuration = new RealmConfiguration.Builder(context).build();
        Realm realm = Realm.getInstance(configuration);
        RealmResults<PersonRealm> personRealms = realm.where(PersonRealm.class).findAll();
        ArrayList<PersonRealm> persons = new ArrayList<>();
        for(PersonRealm p:personRealms) {
            persons.add(p);
        }
        return persons;
    }
    public static ArrayList<Person> sqlRead(Context context) {
        SqlLiteDbHelper helper = new SqlLiteDbHelper(context);
        SQLiteDatabase database = helper.getWritableDatabase();
        Cursor cursor = database.query("simplePersonTable", null, null, null, null, null, null);
        ArrayList<Person> persons = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String secondName = cursor.getString(2);
                Person person = new Person(id,name,secondName);
                persons.add(person);
            }while (cursor.moveToNext());
        }
        return persons;
    }
    public static ArrayList<PersonOrm> greenOrmRead(Context context) {
        SQLiteDatabase db = new DaoMaster.DevOpenHelper(context, "persons-db", null).getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        PersonOrmDao personOrmDao = daoSession.getPersonOrmDao();

        ArrayList<PersonOrm> persons;
        try{
            persons= (ArrayList<PersonOrm>) personOrmDao.loadAll();}
        catch (Exception e) {
            persons = new ArrayList<>();}
        Log.d("size",""+persons.size());
        return  persons;
    }
    public static void clearSql(Context context) {
        SqlLiteDbHelper dbHelper = new SqlLiteDbHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete("simplePersonTable", null, null);
    }
    public static void clearOrm(Context context) {
        SQLiteDatabase db = new DaoMaster.DevOpenHelper(context, "persons-db", null).getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();
        PersonOrmDao personOrmDao = daoSession.getPersonOrmDao();
        personOrmDao.deleteAll();
    }
    public static void clearRealm(Context context) {
        RealmConfiguration configuration = new RealmConfiguration.Builder(context).build();
        Realm realm = Realm.getInstance(configuration);
        realm.beginTransaction();
        realm.clear(PersonRealm.class);
        realm.commitTransaction();
    }
    public static void clearAll(Context context) {
        clearSql(context);
        clearOrm(context);
        clearRealm(context);
    }


}
